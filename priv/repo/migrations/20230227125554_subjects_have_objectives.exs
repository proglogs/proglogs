defmodule ProgLogs.Repo.Migrations.SubjectsHaveObjectives do
  use Ecto.Migration

  def change do
    alter table(:subjects) do
      add :objective_id, references(:objectives, on_delete: :nothing)
    end

    create index(:subjects, [:objective_id])
  end
end
