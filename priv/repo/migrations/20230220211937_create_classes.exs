defmodule ProgLogs.Repo.Migrations.CreateClasses do
  use Ecto.Migration

  def change do
    create table(:classes) do
      add :label, :string, size: 20, null: false

      timestamps()
    end
  end
end
