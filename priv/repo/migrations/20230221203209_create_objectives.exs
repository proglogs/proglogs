defmodule ProgLogs.Repo.Migrations.CreateObjectives do
  use Ecto.Migration

  def change do
    create table(:objectives) do
      add :label, :string, size: 150, null: false
      add :type, :boolean, default: false, null: false
      add :school_level, :string, size: 20, null: false
      add :description, :string, size: 255
      add :subject_id, references(:subjects, on_delete: :nothing)

      timestamps()
    end

    create index(:objectives, [:subject_id])
  end
end
