defmodule ProgLogs.Repo.Migrations.CreateAssessments do
  use Ecto.Migration

  def change do
    create table(:assessments) do
      add :status, :boolean, default: false, null: false
      add :observer, :string, size: 50
      add :student_id, references(:students, on_delete: :delete_all), null: false
      add :objective_id, references(:objectives, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:assessments, [:student_id])
    create index(:assessments, [:objective_id])
  end
end
