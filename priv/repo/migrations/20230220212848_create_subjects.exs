defmodule ProgLogs.Repo.Migrations.CreateSubjects do
  use Ecto.Migration

  def change do
    create table(:subjects) do
      add :label, :string, size: 50, null: false
      add :school_level, :string, size: 20, null: false
      add :description, :string, size: 255
      add :subject_id, references(:subjects, on_delete: :nothing)

      timestamps()
    end

    create index(:subjects, [:subject_id])
  end
end
