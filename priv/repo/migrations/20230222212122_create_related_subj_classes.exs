defmodule ProgLogs.Repo.Migrations.CreateRelatedSubjClasses do
  use Ecto.Migration

  def change do
    create table(:related_subj_classes) do
      add :subject_id, references(:subjects, on_delete: :nothing)
      add :class_id, references(:classes, on_delete: :nothing)

      timestamps()
    end

    create index(:related_subj_classes, [:subject_id])
    create index(:related_subj_classes, [:class_id])
  end
end
