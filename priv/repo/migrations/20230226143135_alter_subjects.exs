defmodule ProgLogs.Repo.Migrations.AlterSubjects do
  use Ecto.Migration

  def change do
    alter table("subjects") do
      modify :school_level, :string, null: false
    end
  end
end
