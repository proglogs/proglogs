defmodule ProgLogs.Repo.Migrations.CreateTeachers do
  use Ecto.Migration

  def change do
    create table(:teachers) do
      add :name, :string, size: 50, null: false
      add :surname, :string, size: 50, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:teachers, [:user_id])
  end
end
