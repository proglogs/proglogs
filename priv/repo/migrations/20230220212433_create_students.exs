defmodule ProgLogs.Repo.Migrations.CreateStudents do
  use Ecto.Migration

  def change do
    create table(:students) do
      add :name, :string, size: 50, null: false
      add :surname, :string, size: 50, null: false
      add :class_id, references(:classes, on_delete: :nothing), null: false
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:students, [:class_id])
    create index(:students, [:user_id])
  end
end
