defmodule ProgLogs.Repo.Migrations.ClassesHaveStudents do
  use Ecto.Migration

  def change do
    alter table(:classes) do
      add :student_id, references(:students, on_delete: :nothing)
    end

    create index(:classes, [:student_id])
  end
end
