defmodule ProgLogs.Repo.Migrations.CreateRelatedTeacherClasses do
  use Ecto.Migration

  def change do
    create table(:related_teacher_classes) do
      add :teacher_id, references(:teachers, on_delete: :nothing)
      add :class_id, references(:classes, on_delete: :nothing)

      timestamps()
    end

    create index(:related_teacher_classes, [:teacher_id])
    create index(:related_teacher_classes, [:class_id])
  end
end
