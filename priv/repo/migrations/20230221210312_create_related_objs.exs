defmodule ProgLogs.Repo.Migrations.CreateRelatedObjs do
  use Ecto.Migration

  def change do
    create table(:related_objs) do
      add :objective_id, references(:objectives, on_delete: :nothing)
      add :related_obj_id, references(:objectives, on_delete: :nothing)

      timestamps()
    end

    create index(:related_objs, [:objective_id])
    create index(:related_objs, [:related_obj_id])

    create unique_index(
             :related_objs,
             [:objective_id, :related_obj_id],
             name: :related_objs_objective_id_related_obj_id_index
           )

    create unique_index(
             :related_objs,
             [:related_obj_id, :objective_id],
             name: :related_objs_related_obj_id_objective_id_index
           )
  end
end
