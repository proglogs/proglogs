defmodule ProgLogsWeb.PageControllerTest do
  use ProgLogsWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to ProgLogs!"
  end
end
