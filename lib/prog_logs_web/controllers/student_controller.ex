defmodule ProgLogsWeb.StudentController do
  use ProgLogsWeb, :controller

  alias ProgLogs.Persons.Student
  alias ProgLogs.Class

  def new(conn, params = %{"class_id" => id}) do
    class = Class.get_class!(id)
    changeset = Student.changeset(%Student{}, params)
    render(conn, "new.html", changeset: changeset, class: class)
  end

  def create(conn, %{"class_id" => class, "student" => student_params}) do
    case Student.create_student(class, student_params) do
      {:ok, _student} ->
        conn
        |> put_flash(:info, "Student added successfully.")
        |> redirect(to: Routes.class_path(conn, :show, class))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:info, "Student added successfully.")
        |> render("show.html", changeset: changeset)
    end
  end

  def show(conn, %{"class_id" => class_id, "id" => id}) do
    student = Student.get_student!(id)
    class = Class.get_class!(class_id)
    render(conn, "show.html", student: student, class: class)
  end

  def edit(conn, %{"class_id" => class_id, "id" => id}) do
    student = Student.get_student!(id)
    class = Class.get_class!(class_id)
    changeset = Student.change_student(student)
    render(conn, "edit.html", student: student, class: class, changeset: changeset)
  end

  def update(conn, %{"id" => id, "class_id" => class_id, "student" => student_params}) do
    student = Student.get_student!(id)
    class = Class.get_class!(class_id)

    case Student.update_student(student, student_params) do
      {:ok, student} ->
        conn
        |> put_flash(:info, "Student updated successfully.")
        |> redirect(to: Routes.class_student_path(conn, :show, class, student))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", class: class, student: student, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id, "class_id" => class_id}) do
    student = Student.get_student!(id)
    class = Class.get_class!(class_id)
    {:ok, _student} = Student.delete_student(student)

    conn
    |> put_flash(:info, "Student deleted successfully.")
    |> redirect(to: Routes.class_path(conn, :show, class))
  end
end
