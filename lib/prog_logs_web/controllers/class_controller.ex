defmodule ProgLogsWeb.ClassController do
  use ProgLogsWeb, :controller

  alias ProgLogs.Class
  alias ProgLogs.Repo
  alias ProgLogs.Persons.Student

  def index(conn, _params) do
    classes = Class.list_classes()
    render(conn, "index.html", classes: classes)
  end

  def new(conn, params) do
    changeset = Class.changeset(%Class{}, params)
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"class" => class_params}) do
    case Class.create_class(class_params) do
      {:ok, _class} ->
        conn
        |> put_flash(:info, "Class created successfully.")
        |> redirect(to: Routes.class_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Class not created.")
        |> render("new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    class = Class.get_class!(id)
    students = Student.list_students(class)
    render(conn, "show.html", class: class, students: students)
  end

  def edit(conn, %{"id" => id}) do
    class = Class.get_class!(id)
    changeset = Class.change_class(class)
    render(conn, "edit.html", class: class, changeset: changeset)
  end

  def update(conn, %{"id" => id, "class" => class_params}) do
    class = Class.get_class!(id)

    case Class.update_class(class, class_params) do
      {:ok, class} ->
        conn
        |> put_flash(:info, "Class updated successfully.")
        |> redirect(to: Routes.class_path(conn, :show, class))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", class: class, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    class = Class.get_class!(id)
    {:ok, _class} = Repo.delete(class)

    conn
    |> put_flash(:info, "Class deleted successfully.")
    |> redirect(to: Routes.class_path(conn, :index))
  end
end
