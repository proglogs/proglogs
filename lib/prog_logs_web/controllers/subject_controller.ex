defmodule ProgLogsWeb.SubjectController do
  use ProgLogsWeb, :controller

  alias ProgLogs.Curriculum.Subject
  alias ProgLogs.Curriculum.Objective

  def index(conn, _params) do
    subjects = Subject.list_subjects()
    render(conn, "index.html", subjects: subjects)
  end

  def new(conn, params) do
    changeset = Subject.changeset(%Subject{}, params)
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"subject" => subject_params}) do
    case Subject.create_subject(subject_params) do
      {:ok, _subject} ->
        conn
        |> put_flash(:info, "Subject created successfully.")
        |> redirect(to: Routes.subject_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Subject not created.")
        |> render("new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    subject = Subject.get_subject!(id)
    objectives = Objective.list_objectives(subject)
    render(conn, "show.html", subject: subject, objectives: objectives)
  end

  def edit(conn, %{"id" => id}) do
    subject = Subject.get_subject!(id)
    changeset = Subject.change_subject(subject)
    render(conn, "edit.html", subject: subject, changeset: changeset)
  end

  def update(conn, %{"id" => id, "subject" => subject_params}) do
    subject = Subject.get_subject!(id)

    case Subject.update_subject(subject, subject_params) do
      {:ok, subject} ->
        conn
        |> put_flash(:info, "Subject updated successfully.")
        |> redirect(to: Routes.subject_path(conn, :show, subject))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", subject: subject, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    subject = Subject.get_subject!(id)
    {:ok, _subject} = Subject.delete_subject(subject)

    conn
    |> put_flash(:info, "Room deleted successfully.")
    |> redirect(to: Routes.subject_path(conn, :index))
  end
end
