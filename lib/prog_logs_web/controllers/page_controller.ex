defmodule ProgLogsWeb.PageController do
  use ProgLogsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
