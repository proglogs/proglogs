defmodule ProgLogsWeb.ObjectiveController do
  use ProgLogsWeb, :controller

  alias ProgLogs.Curriculum.Objective
  alias ProgLogs.Curriculum.Subject

  def new(conn, params = %{"subject_id" => id}) do
    subject = Subject.get_subject!(id)
    changeset = Objective.changeset(%Objective{}, params)
    render(conn, "new.html", changeset: changeset, subject: subject)
  end

  def create(conn, %{"subject_id" => subject, "objective" => objective_params}) do
    case Objective.create_objective(subject, objective_params) do
      {:ok, _objective} ->
        conn
        |> put_flash(:info, "Objective created successfully.")
        |> redirect(to: Routes.subject_path(conn, :show, subject))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Objective not created.")
        |> render("new.html", changeset: changeset)
    end
  end

  def show(conn, %{"subject_id" => subject_id, "id" => id}) do
    objective = Objective.get_objective!(id)
    subject = Subject.get_subject!(subject_id)
    render(conn, "show.html", objective: objective, subject: subject)
  end

  def edit(conn, %{"subject_id" => subject_id, "id" => id}) do
    objective = Objective.get_objective!(id)
    subject = Subject.get_subject!(subject_id)
    changeset = Objective.change_objective(objective)
    render(conn, "edit.html", objective: objective, subject: subject, changeset: changeset)
  end

  def update(conn, %{"id" => id, "subject_id" => subject_id, "objective" => objective_params}) do
    objective = Objective.get_objective!(id)
    subject = Subject.get_subject!(subject_id)

    case Objective.update_objective(objective, objective_params) do
      {:ok, objective} ->
        conn
        |> put_flash(:info, "Objective updated successfully.")
        |> redirect(to: Routes.subject_objective_path(conn, :show, subject, objective))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", subject: subject, objective: objective, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id, "subject_id" => subject_id}) do
    objective = Objective.get_objective!(id)
    subject = Subject.get_subject!(subject_id)
    {:ok, _objective} = Objective.delete_objective(objective)

    conn
    |> put_flash(:info, "Objective deleted successfully.")
    |> redirect(to: Routes.subject_path(conn, :show, subject))
  end
end
