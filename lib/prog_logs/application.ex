defmodule ProgLogs.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      ProgLogs.Repo,
      # Start the Telemetry supervisor
      ProgLogsWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: ProgLogs.PubSub},
      # Start the Endpoint (http/https)
      ProgLogsWeb.Endpoint
      # Start a worker by calling: ProgLogs.Worker.start_link(arg)
      # {ProgLogs.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ProgLogs.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    ProgLogsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
