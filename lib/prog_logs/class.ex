defmodule ProgLogs.Class do
  use Ecto.Schema
  import Ecto.Changeset

  alias ProgLogs.Repo
  alias ProgLogs.Class
  alias ProgLogs.Persons.Student

  schema "classes" do
    field :label, :string
    has_many :students, Student

    timestamps()
  end

  def list_classes do
    Repo.all(Class)
  end

  def get_class!(id), do: Repo.get!(Class, id)

  def create_class(attrs \\ %{}) do
    %Class{}
    |> Class.changeset(attrs)
    |> Repo.insert()
  end

  def change_class(%Class{} = class) do
    Class.changeset(class, %{})
  end

  def update_class(%Class{} = class, attrs) do
    class
    |> Class.changeset(attrs)
    |> Repo.update()
  end

  def delete_class(%Class{} = class) do
    Repo.delete(class)
  end

  @doc false
  def changeset(class, attrs) do
    class
    |> cast(attrs, [:label])
    |> validate_required([:label])
  end
end
