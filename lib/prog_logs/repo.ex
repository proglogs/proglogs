defmodule ProgLogs.Repo do
  use Ecto.Repo,
    otp_app: :prog_logs,
    adapter: Ecto.Adapters.Postgres
end
