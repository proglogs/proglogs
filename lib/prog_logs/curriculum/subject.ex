defmodule ProgLogs.Curriculum.Subject do
  use Ecto.Schema
  import Ecto.Changeset

  alias ProgLogs.Repo
  alias ProgLogs.Curriculum.Subject
  alias ProgLogs.Curriculum.Objective

  schema "subjects" do
    field :description, :string
    field :label, :string
    field :school_level, :string
    field :subject_id, :id
    has_many :objectives, Objective

    timestamps()
  end

  def create_subject(attrs \\ %{}) do
    %Subject{}
    |> Subject.changeset(attrs)
    |> Repo.insert()
  end

  def list_subjects do
    Repo.all(Subject)
  end

  def get_subject!(id), do: Repo.get!(Subject, id)

  def change_subject(%Subject{} = subject) do
    Subject.changeset(subject, %{})
  end

  def update_subject(%Subject{} = subject, attrs) do
    subject
    |> Subject.changeset(attrs)
    |> Repo.update()
  end

  def delete_subject(%Subject{} = subject) do
    Repo.delete(subject)
  end

  @doc false
  def changeset(subject, attrs) do
    subject
    |> cast(attrs, [:label, :school_level, :description])
    |> validate_required([:label, :school_level, :description])
  end
end
