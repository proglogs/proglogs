defmodule ProgLogs.Curriculum.RelatedSubjClass do
  use Ecto.Schema
  import Ecto.Changeset

  schema "related_subj_classes" do
    field :subject_id, :id
    field :class_id, :id

    timestamps()
  end

  @doc false
  def changeset(related_subj_class, attrs) do
    related_subj_class
    |> cast(attrs, [])
    |> validate_required([])
  end
end
