defmodule ProgLogs.Curriculum.Objective do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias ProgLogs.Curriculum.RelatedObj
  alias ProgLogs.Curriculum.Objective
  alias ProgLogs.Repo
  alias ProgLogs.Curriculum.Subject

  schema "objectives" do
    field :description, :string
    field :label, :string
    field :school_level, :string
    field :type, :boolean, default: false
    belongs_to :subject, Subject

    many_to_many :related_objs,
                 Objective,
                 join_through: RelatedObj,
                 join_keys: [objective_id: :id, related_objs_id: :id]

    many_to_many :reverse_related_objs,
                 Objective,
                 join_through: RelatedObj,
                 join_keys: [related_objs_id: :id, objective_id: :id]

    timestamps()
  end

  def create_objective(id, attrs \\ %{}) do
    subject = Subject.get_subject!(id)

    subject
    |> Ecto.build_assoc(:objectives)
    |> Objective.changeset(attrs)
    |> Repo.insert()
  end

  def get_objective!(id), do: Repo.get!(Objective, id)

  def list_objectives(subject) do
    Repo.all(
      from obj in Objective,
        join: subject in assoc(obj, :subject),
        where: obj.subject_id == ^subject.id
    )
  end

  def change_objective(%Objective{} = objective) do
    Objective.changeset(objective, %{})
  end

  def update_objective(%Objective{} = objective, attrs) do
    objective
    |> Objective.changeset(attrs)
    |> Repo.update()
  end

  def delete_objective(%Objective{} = objective) do
    Repo.delete(objective)
  end

  @doc false
  def changeset(objective, attrs) do
    objective
    |> cast(attrs, [:label, :type, :school_level, :description])
    |> validate_required([:label, :type, :school_level, :description])
  end
end
