defmodule ProgLogs.Curriculum.RelatedObj do
  use Ecto.Schema
  import Ecto.Changeset

  schema "related_objs" do
    field :objective_id, :id
    field :related_obj_id, :id

    timestamps()
  end

  @doc false
  def changeset(related_obj, attrs) do
    related_obj
    |> cast(attrs, [])
    |> unique_constraint(
      [:objective_id, :related_obj_id],
      name: :related_objs_objective_id_related_obj_id_index
    )
    |> unique_constraint(
      [:related_obj_id, :objective_id],
      name: :related_objs_related_obj_id_objective_id_index
    )
    |> validate_required([])
  end
end
