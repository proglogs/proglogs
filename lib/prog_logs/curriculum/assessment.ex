defmodule ProgLogs.Curriculum.Assessment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "assessments" do
    field :observer, :string
    field :status, :boolean, default: false
    field :student_id, :id
    field :objective_id, :id

    timestamps()
  end

  @doc false
  def changeset(assessment, attrs) do
    assessment
    |> cast(attrs, [:status, :observer])
    |> validate_required([:status, :observer])
  end
end
