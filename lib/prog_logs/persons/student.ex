defmodule ProgLogs.Persons.Student do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias ProgLogs.Repo
  alias ProgLogs.Persons.Student
  alias ProgLogs.Class

  schema "students" do
    field :name, :string
    field :surname, :string
    field :user_id, :id
    belongs_to :class, Class

    timestamps()
  end

  def create_student(id, attrs \\ %{}) do
    class = Class.get_class!(id)

    class
    |> Ecto.build_assoc(:students)
    |> Student.changeset(attrs)
    |> Repo.insert()
  end

  def get_student!(id), do: Repo.get!(Student, id)

  def list_students(class) do
    Repo.all(
      from std in Student,
        join: class in assoc(std, :class),
        where: std.class_id == ^class.id
    )
  end

  def change_student(%Student{} = student) do
    Student.changeset(student, %{})
  end

  def update_student(%Student{} = student, attrs) do
    student
    |> Student.changeset(attrs)
    |> Repo.update()
  end

  def delete_student(%Student{} = student) do
    Repo.delete(student)
  end

  @doc false
  def changeset(student, attrs) do
    student
    |> cast(attrs, [:name, :surname])
    |> validate_required([:name, :surname])
  end
end
