defmodule ProgLogs.Persons.RelatedTeacherClass do
  use Ecto.Schema
  import Ecto.Changeset

  schema "related_teacher_classes" do
    field :teacher_id, :id
    field :class_id, :id

    timestamps()
  end

  @doc false
  def changeset(related_teacher_class, attrs) do
    related_teacher_class
    |> cast(attrs, [])
    |> validate_required([])
  end
end
