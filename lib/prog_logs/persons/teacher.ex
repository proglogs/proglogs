defmodule ProgLogs.Persons.Teacher do
  use Ecto.Schema
  import Ecto.Changeset

  schema "teachers" do
    field :name, :string
    field :surname, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(teacher, attrs) do
    teacher
    |> cast(attrs, [:name, :surname])
    |> validate_required([:name, :surname])
  end
end
